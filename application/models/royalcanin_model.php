<?php
/**
 * Created by PhpStorm.
 * User: gilangsonar
 * Date: 10/16/15
 * Time: 18:11
 */

class Royalcanin_model extends CI_Controller{

    var $db_user = "royalcanin__user";
    var $db_user_session = "royalcanin__user_session";
    var $db_question = "royalcanin__question";
    var $db_answer = "royalcanin__answer";
    var $db_share = "royalcanin__share";
    var $rel_user_share = "royalcanin__user_share";

    function __construct(){
        parent::__construct();
    }

    function count_session_answer_today($periode){
        $sql = "
                SELECT count(*) as jumlah
                FROM $this->db_answer a
                WHERE 1
                    AND a.session_id = ?
                    AND a.periode = ?
                    AND DATE(a.datetime) = CURDATE()
                LIMIT 1
                ";
        $query = $this->db->query($sql, array($this->session->userdata('session_id'),$periode))->row()->jumlah;
        if(empty($query)){
            return FALSE;
        }else{
            return $query;
        }
    }

    function check_answer_question_session($question_id){
        $sql = "
                SELECT *
                FROM $this->db_answer a
                WHERE 1
                    AND a.session_id = ?
                    AND a.question_id = ?
                    AND DATE(a.datetime) = CURDATE()
                ";
        $query = $this->db->query($sql, array($this->session->userdata('session_id'), $question_id))->result();
        if(empty($query)){
            return FALSE;
        }else{
            return $query;
        }
    }

    function count_user_failed_session($periode){
        $sql = "
                SELECT count(*) as jumlah
                FROM $this->db_answer a
                WHERE 1
                    AND a.session_id = ?
                    AND a.periode = ?
                    AND DATE(a.datetime) = CURDATE()
                    AND a.status_answer = 'false'
                LIMIT 1
                ";
        $query = $this->db->query($sql, array($this->session->userdata('session_id'),$periode))->row()->jumlah;
        if(empty($query)){
            return FALSE;
        }else{
            return $query;
        }
    }

    function count_user_hint_session($periode){
        $sql = "
                SELECT count(*) as jumlah
                FROM $this->db_answer a
                WHERE 1
                    AND a.session_id = ?
                    AND a.periode = ?
                    AND DATE(a.datetime) = CURDATE()
                    AND a.hint = 'yes'
                LIMIT 1
                ";
        $query = $this->db->query($sql, array($this->session->userdata('session_id'),$periode))->row()->jumlah;
        if(empty($query)){
            return FALSE;
        }else{
            return $query;
        }
    }

    /****** END SESSION ******/

    function count_user_answer_today($periode){
        $sql = "
				SELECT count(*) as jumlah
				FROM $this->db_answer a
				LEFT JOIN $this->db_user b
					ON a.user_id = b.user_id
				WHERE 1
					AND a.user_id = ?
					AND a.periode = ?
					AND DATE(a.datetime) = CURDATE()
				LIMIT 1
				";
        $query = $this->db->query($sql, array($this->session->userdata('user_id'),$periode))->row()->jumlah;
        if(empty($query)){
            return FALSE;
        }else{
            return $query;
        }
    }

    function count_user_failed($periode){
        $sql = "
				SELECT count(*) as jumlah
				FROM $this->db_answer a
				LEFT JOIN $this->db_user b
					ON a.user_id = b.user_id
				WHERE 1
					AND a.user_id = ?
					AND a.periode = ?
					AND DATE(a.datetime) = CURDATE()
					AND a.status_answer = 'false' OR a.status_answer is NULL
				LIMIT 1
				";
        $query = $this->db->query($sql, array($this->session->userdata('user_id'),$periode))->row()->jumlah;
        if(empty($query)){
            return FALSE;
        }else{
            return $query;
        }
    }

    function count_user_hint($periode){
        $sql = "
				SELECT count(*) as jumlah
				FROM $this->db_answer a
				LEFT JOIN $this->db_user b
					ON a.user_id = b.user_id
				WHERE 1
					AND a.user_id = ?
					AND a.periode = ?
					AND DATE(a.datetime) = CURDATE()
					AND a.hint = 'yes'
				LIMIT 1
				";
        $query = $this->db->query($sql, array($this->session->userdata('user_id'),$periode))->row()->jumlah;
        if(empty($query)){
            return FALSE;
        }else{
            return $query;
        }
    }

    function get_random_question($category,$periode){
        $sql = "
				SELECT a.question_id
				FROM $this->db_question a
				WHERE 1
				AND a.question_category = ?
				AND a.question_periode = ?
				ORDER BY RAND()
				LIMIT 10
				";
        $query = $this->db->query($sql, array($category,$periode))->result();
        if(empty($query)){
            return FALSE;
        }else{
            return $query;
        }
    }

    function check_answer_question($question_id){
        $sql = "
				SELECT *
				FROM $this->db_answer a
				JOIN $this->db_user b
					ON a.user_id = b.user_id
				WHERE 1
					AND a.user_id = ?
					AND a.question_id = ?
					AND DATE(a.datetime) = CURDATE()
				";
        $query = $this->db->query($sql, array($this->session->userdata('user_id'), $question_id))->result();
        if(empty($query)){
            return FALSE;
        }else{
            return $query;
        }
    }

    function detail_question($question_id){
        $sql = "
				SELECT *
				FROM $this->db_question a
				WHERE 1
					AND a.question_id = ?
				";
        $query = $this->db->query($sql, array($question_id))->row();
        if(empty($query)){
            return FALSE;
        }else{
            return $query;
        }
    }

    function get_point_today($periode){
        $sql = "
				SELECT SUM(point) as point
				FROM $this->db_answer a
				JOIN $this->db_user_session c
                    ON a.session_id = c.session_id    
                JOIN $this->db_user b
					ON c.user_id = b.user_id
                WHERE 1
					AND c.user_id = ?
					AND a.question_id < 1000
					AND a.periode = ?
				";
        $query = $this->db->query($sql, array($this->session->userdata('user_id'),$periode))->row()->point;
        if(empty($query)){
            return FALSE;
        }else{
            return $query;
        }
    }

    function get_total_point($periode){
        $sql = "
				SELECT SUM(point) as point
				FROM $this->db_answer a
				JOIN $this->db_user_session c
                    ON a.session_id = c.session_id
                JOIN $this->db_user b
					ON c.user_id = b.user_id
                WHERE 1
					AND c.user_id = ?
					AND a.periode = ?
				";
        $query = $this->db->query($sql, array($this->session->userdata('user_id'),$periode))->row()->point;
        if(empty($query)){
            return FALSE;
        }else{
            return $query;
        }
    }

    function get_session_point($periode){
        $sql = "
				SELECT SUM(point) as point
				FROM $this->db_answer a
				JOIN $this->db_user_session c
                    ON a.session_id = c.session_id
                JOIN $this->db_user b
					ON c.user_id = b.user_id
                WHERE 1
					AND a.session_id = ?
					AND a.periode = ?
				";
        $query = $this->db->query($sql, array($this->session->userdata('session_id'),$periode))->row()->point;
        if(empty($query)){
            return FALSE;
        }else{
            return $query;
        }
    }

    function get_user_detail($email){
        $sql = "
				SELECT *
				FROM $this->db_user a
				WHERE 1
					AND a.email = ?
				LIMIT 1
				";
        $query = $this->db->query($sql, array($email))->row();
        if(empty($query)){
            return FALSE;
        }else{
            return $query;
        }
    }

    function get_user_periode($periode, $user_id){
        $sql = "
                SELECT *
                FROM $this->db_user_session a
                WHERE 1
                    AND a.question_periode = ?
                    AND a.user_id = ?
                LIMIT 1
                ";
        $query = $this->db->query($sql, array($periode, $user_id))->row();
        if(empty($query)){
            return FALSE;
        }else{
            return $query;
        }
    }

    function check_bonus_share($user_answer){
        $sql = "
				SELECT *
				FROM $this->db_answer a
				JOIN $this->db_user b
					ON a.user_id = b.user_id
				WHERE 1
					AND a.user_id = ?
					AND a.question_id = ?
					AND a.user_answer = ?
					AND DATE(a.datetime) = CURDATE()
				";
        $query = $this->db->query($sql, array($this->session->userdata('user_id'), 1000, $user_answer))->result();
        if(empty($query)){
            return FALSE;
        }else{
            return $query;
        }
    }

    function get_share_item(){
        $sql = "
                select *
                from royalcanin__share
                where share_id NOT IN (
                select share_id from royalcanin__user_share
                where user_id = ?
                )
                order by share_id DESC
                limit 4
				";
        $query = $this->db->query($sql,array($this->session->userdata('user_id')))->result();
        if(empty($query)){
            return FALSE;
        }else{
            return $query;
        }
    }

    function get_user_skor($periode){
        return $this->db->query(
            "
				select b.session_id,c.user_id, c.nama_lengkap,c.email,c.phone, sum(a.point) as poin, e.question_category
                from `royalcanin__answer` a
                left join `royalcanin__user_session` b
                on a.session_id=b.session_id
                left JOIN `royalcanin__user` c
                on b.user_id=c.user_id
                left join `royalcanin__question` e
                on a.question_id=e.question_id

                where
                c.email is not null
                and a.periode = '$periode'
                and a.question_id < '1000'
                GROUP BY a.session_id

                order by c.user_id asc
				"
        )->result();
    }

    function get_total_skor($periode){
        return $this->db->query("
                select x.session_id,z.user_id, sum(x.point) as total_poin
                from `royalcanin__answer` x
                left join `royalcanin__user_session` y
                on x.session_id=y.session_id
                left JOIN `royalcanin__user` z
                on y.user_id=z.user_id
                where
                z.email is not null
                and x.periode = '$periode'
                GROUP BY x.session_id
        ")->result();
    }

    function get_point_history($sess_id){
        return $this->db->query("
            select a.*,c.nama_lengkap from royalcanin__answer a
            left join royalcanin__user_session b on a.session_id=b.session_id
            left JOIN `royalcanin__user` c on b.user_id=c.user_id
            where a.session_id = '$sess_id'
        ")->result();
    }

}