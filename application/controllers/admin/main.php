<?php
/**
 * Created by PhpStorm.
 * User: gilangsonar
 * Date: 11/8/15
 * Time: 1:55
 */

class Main extends CI_Controller{

    var $periode = 1;

    function __construct(){
        parent::__construct();
        $this->load->model('royalcanin_model');
    }

    function users(){
        $data=array(
            'users'         => $this->royalcanin_model->get_user_skor($this->periode),
            'total_skor'    => $this->royalcanin_model->get_total_skor($this->periode),
            'period'        => $this->periode,
            'content'       => 'admin/index'
        );
        $this->load->view('admin/general/container',$data);
    }

    function change_periode(){
        $periode = $this->input->post('periode');
        $data=array(
            'users'         => $this->royalcanin_model->get_user_skor($periode),
            'total_skor'    => $this->royalcanin_model->get_total_skor($periode),
            'period'        => $periode,
        );
        $this->load->view('admin/loop_view',$data);
    }

    function history($sess_id){
        $data=array(
            'users'     => $this->royalcanin_model->get_point_history($sess_id),
            'period'    => $this->periode,
            'content'   => 'admin/point_history'
        );
        $this->load->view('admin/general/container',$data);
    }
}