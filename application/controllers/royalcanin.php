<?php
/**
 * Created by PhpStorm.
 * User: gilangsonar
 * Date: 10/15/15
 * Time: 19:50
 */

class Royalcanin extends CI_Controller{

    var $data = array();
    var $periode = 5;
    
    function __construct(){
        parent::__construct();
        header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
        $this->load->library('facebook_lib');
        $this->data['config_royalcanin'] =  $this->config->item('royalcanin');

        $this->load->model('royalcanin_model');
        $this->load->library('facebook_lib');
        $this->load->helper('download');
    }

    function index(){
        $this->session->unset_userdata('question');
        $this->data['content'] = 'index';
        $this->load->view('general/theme',$this->data);
    }

    function choice(){
        $user_periode = $this->royalcanin_model->get_user_periode($this->periode, $this->session->userdata('user_id'));
        if(!empty($user_periode)){
            $this->session->set_flashdata('alert','Anda sudah pernah menjawab periode ini, main lagi minggu depan!');
            redirect(base_url().'result');
        }
        $this->data['content'] = 'choice';
        $this->load->view('general/theme',$this->data);
    }

    function play($category=1){
        $play = $this->session->userdata('play');
        if($play == 'no')
            redirect('register');

        $this->load->model('royalcanin_model');
        $this->session->set_userdata('user_category',$category);

        $q = $this->input->get('q');
        if(empty($q)) $q = 1;
        $this->data['now_question'] = $q;

        $total_answer = $random_question = $this->royalcanin_model->count_session_answer_today($this->periode);
        if($this->data['now_question'] > 10 || $total_answer >= 10)
            redirect('result');

        if(!$this->session->userdata('question')){
            $random_question = $this->royalcanin_model->get_random_question($category,$this->periode);
            $this->session->set_userdata('question', $random_question);
        }

        $question = $this->session->userdata('question');
        $index = $q - 1;
        $question_id = $question[$index]->question_id;
        $answer = $this->royalcanin_model->check_answer_question_session($question_id);
        $failed_answer = $this->royalcanin_model->count_user_failed_session($this->periode);
        $total_hint = $this->royalcanin_model->count_user_hint_session($this->periode);
        if(!empty($answer)) redirect(base_url().'play/'.$category.'?q='.($this->data['now_question'] + 1));
        
        if($failed_answer+$total_hint >= 3){
            $this->session->set_flashdata('alert','Total kesempatan Anda sudah habis');
            redirect(base_url().'register');
        } 
        
        $data_answer = array(
            "periode"		=> $this->periode,
            "datetime"		=> date('Y-m-d H:i:s'),
            "session_id"	=> $this->session->userdata('session_id'),
            "question_id"	=> $question_id,
            "user_answer"	=> '',
            "point"			=> 0,
        );
        if(empty($answer)){
            $this->db->insert('royalcanin__answer', $data_answer);
        }

        $this->data['detail_question']  = $this->royalcanin_model->detail_question($question_id);
        $this->data['question']         = $question;
        $this->data['total_failed']     = $failed_answer;
        $this->data['total_hint']       = $total_hint;
        $this->data['content']          = 'play';
        $this->load->view('general/theme',$this->data);
    }

    function answer(){
        $postsubmit = $this->input->post('postsubmit');
        $question_id = $this->input->post('question_id');
        $user_answer = $this->input->post('user_answer');
        $answer = $this->royalcanin_model->check_answer_question_session($question_id);

        $point = 10;
        $detail_question = $this->royalcanin_model->detail_question($question_id);
        if($detail_question->question_answer !== $user_answer)
            $point = 0;

        if($postsubmit !== 'true'){
            $point = 0;
        }

        if(empty($answer->user_answer)){
            if($detail_question->question_answer == $user_answer){
                $status_answer = 'true';
            }elseif($detail_question->question_answer !== $user_answer){
                $status_answer = 'false';
            }

            if($this->session->flashdata('hint') == true){
                $hint = 'yes';
            }else{
                $hint = 'no';
            }

            $data_answer = array(
                "datetime"		=> date('Y-m-d H:i:s'),
                "user_answer"	=> $user_answer,
                "point"			=> $point,
                'status_answer' => $status_answer,
                'hint'          => $hint
            );
            $date_now = date('Y-m-d');
            $data_where = array(
                "periode"			=> $this->periode,
                "session_id"		=> $this->session->userdata('session_id'),
                "question_id"		=> $question_id,
                "DATE(datetime)"	=> $date_now
            );
            $this->db->where($data_where);
            $this->db->update('royalcanin__answer', $data_answer);

            if($detail_question->question_answer == $user_answer){
                $this->session->set_userdata('q_'.$detail_question->question_id,'true');
                echo "true";
            }elseif($detail_question->question_answer !== $user_answer){
                $this->session->set_userdata('q_'.$detail_question->question_id,'false');
                echo "false";
            }
            exit;

        }else{

            echo "error";
            exit;
        }

    }

    function register(){
        $check_timer = $this->input->get('timer');
        if($check_timer == "empty")
            $this->session->userdata('play','no');

        $this->load->library('form_validation');
        $config =   array(
            array("field"=>"nama_lengkap","label" =>"Nama Lengkap", "rules"=>"xss_clean|required"),
            array("field"=>"phone","label" =>"Telp./Hp", "rules"=>"xss_clean|required"),
            array("field"=>"email","label" =>"Email", "rules"=>"xss_clean|required"),
        );
        $this->form_validation->set_rules($config);
        if($this->form_validation->run()){

            $data_user = array(
                "nama_lengkap"	=>	$this->input->post('nama_lengkap'),
                "phone"			=>	$this->input->post('phone'),
                "email"			=>	$this->input->post('email'),
                "datetime"      =>	date('Y-m-d H:i:s'),
            );

            $this->data['user_detail'] = $user_detail = $this->royalcanin_model->get_user_detail($this->input->post('email'));
            if(empty($user_detail)) :
                $this->db->insert('royalcanin__user', $data_user);
                $user_id = $this->db->insert_id();
            else :
                $this->db->where('email', $this->input->post('email'));
                $this->db->update('royalcanin__user', $data_user);
                $user_id = $user_detail->user_id;
            endif;    
        
            $this->session->set_userdata('user_id', $user_id);
            $user_periode = $this->royalcanin_model->get_user_periode($this->periode, $user_id);
            if(empty($user_periode)){
                $this->db->insert('royalcanin__user_session', array('session_id' => $this->session->userdata('session_id'), 'user_id' => $user_id, 'question_periode' => $this->periode));
            }else{
                $this->session->set_flashdata('alert','Anda sudah pernah menjawab periode ini, main lagi minggu depan!');
            }    

			/* SEND EMAIL */
            $displayname = "Royal Canin Indonesia";
            $default_mailer = 'digitalmarketing.id@royalcanin.com';
            $config = array(
                'protocol'  => 'smtp',
                'mailtype'  => 'html',
                'charset'   => 'utf-8',
                'smtp_host' => 'localhost',
                'smtp_port' =>  25,
                'smtp_user' => 'no-reply@codemade.co.id',
                'smtp_pass' => 'cod88092',
                'priority'  => 3, // 1, 2, 3, 4, 5    Email Priority. 1 => highest. 5 => lowest. 3 => normal.

                /*
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => 465,
                'smtp_user' => 'dummygilang@gmail.com',
                'smtp_pass' => 'sonaramanu'
                */
            );
            $this->load->library('email');
            $this->email->initialize($config);
            $this->email->set_newline("\r\n");
            $this->email->to($this->input->post('email'));
            $this->email->from($default_mailer, $displayname);
            $this->email->subject('Royal Canin Indonesia');

            $mail_msg='';
            $point = $this->royalcanin_model->get_session_point($this->periode);

            if($point >= 0 && $point <= 40) :
                if($this->session->userdata('user_category') == '1') :
                    $mail_msg['msg_view'] = 'email/good/dog';
                elseif($this->session->userdata('user_category') == '2'):
                    $mail_msg['msg_view'] = 'email/good/cat';
                endif;

            elseif($point >= 50 && $point <= 70):
                if($this->session->userdata('user_category') == '1') :
                    $mail_msg['msg_view'] = 'email/great/dog';
                elseif($this->session->userdata('user_category') == '2'):
                    $mail_msg['msg_view'] = 'email/great/cat';
                endif;

            elseif($point >= 80 && $point <= 100):
                if($this->session->userdata('user_category') == '1') :
                    $mail_msg['msg_view'] = 'email/super/dog';
                elseif($this->session->userdata('user_category') == '2'):
                    $mail_msg['msg_view'] = 'email/super/cat';
                endif;

            endif;
            $message =  $this->load->view('email/template', $mail_msg, true);
            $this->email->message($message);
            $this->email->send();
            $this->session->set_flashdata('register-success', TRUE);
            redirect(base_url().'result');

        }else{
            $user_periode = $this->royalcanin_model->get_user_periode($this->periode, $this->session->userdata('user_id'));
            if(!empty($user_periode)){
                $this->session->set_flashdata('alert','Anda sudah pernah menjawab periode ini, main lagi minggu depan!');
                redirect(base_url().'result');
            }
            $this->data['content'] = 'register';
            $this->load->view('general/theme',$this->data);
        }
    }

    function result(){
        $user_id = $this->session->userdata('user_id');    
        if(empty($user_id)){
            $this->session->set_flashdata('alert','Anda harus mengisi data diri.');
            redirect(base_url().'register');
        }

        $this->data['point_today']  = (int)$this->royalcanin_model->get_point_today($this->periode);
        $this->data['total_point']  = (int)$this->royalcanin_model->get_total_point($this->periode);
        $this->data['content']      = 'result';
        $this->load->view('general/theme',$this->data);
    }

    function result2(){
        $user_id = $this->session->userdata('user_id');
        if(empty($user_id)){
            $this->session->set_flashdata('alert','Anda harus mengisi data diri.');
            redirect(base_url().'register');
        }
        $this->data['point_today']  = (int)$this->royalcanin_model->get_point_today($this->periode);
        $this->data['total_point']  = (int)$this->royalcanin_model->get_total_point($this->periode);
        $this->data['content']      = 'result2';
        $this->load->view('general/theme',$this->data);
    }

    function share(){
        $sharing = count($this->royalcanin_model->get_share_item());
        if($sharing > 1){
            $this->data['content']      = 'share';
            $this->data['share_item']   = $this->royalcanin_model->get_share_item();
            $this->load->view('general/theme',$this->data);
        }else{
            $this->session->set_flashdata('alert','Anda sudah share seluruh content untuk menambah poin.');
            redirect('result');
        }
    }

    function bonus_share(){
        $user_answer = $this->input->post('user_answer'); // f atau s
        if($user_answer == 'f'){
            $data_bonus = array(
                "periode"		=> $this->periode,
                "datetime"		=> date('Y-m-d H:i:s'),
                "session_id"	=> $this->session->userdata('session_id'),
                "question_id"	=> 1000,
                "user_answer"	=> $user_answer,
                "point"			=> 1,
            );
            $this->db->insert('royalcanin__answer', $data_bonus);
            echo "success";
            exit;
        }elseif($user_answer == 's'){
            $data_bonus = array(
                "periode"		=> $this->periode,
                "datetime"		=> date('Y-m-d H:i:s'),
                "session_id"	=> $this->session->userdata('session_id'),
                "question_id"	=> 2000,
                "user_answer"	=> $user_answer,
                "point"			=> 1,
            );
            $this->db->insert('royalcanin__answer', $data_bonus);

            $data_share = array(
                "user_id"	=> $this->session->userdata('user_id'),
                "share_id"	=> $this->input->post('share_id'),
            );
            $this->db->insert('royalcanin__user_share', $data_share);
            echo "success";
            exit;
        }

    }

    function hint(){
        $this->session->set_flashdata('hint',true);
        $question_id = $this->input->post('question_id');
        $this->data['detail_question']  = $this->royalcanin_model->detail_question($question_id);
        $this->load->view('hint',$this->data);
    }

    function download($file) {
        $data = file_get_contents("./file/".$file);
        force_download(str_replace('_',' ',$file), $data);
    }

    /*========================= ADMIN ============================*/
    function __rule_admin(){
        if(!$this->royalcanin_model->is_admin()){
            redirect(base_url().'royalcanin');
        }
    }

    function listuser($view = ''){
        $this->__rule_admin();
        if($view == 'data'){
            $this->data['alluser'] = $this->royalcanin_model->get_all_user();
            $data['page'] = 1;
            $data['total'] = count($this->data['alluser']);
            $data['rows'] = $this->data['alluser'];
            header('Content-type: application/json');
            echo json_encode($data);
        }else{
            $this->data['content']  = $this->load->view('admin/listuser', $this->data, true);
            $this->load->view('admin/theme_admin', $this->data);
        }
    }

    function listjawara($view = ''){
        $periode = $this->input->get('periode');
        if(empty($periode)) $periode = 1;
        $this->data['periode'] = $periode;
        $this->__rule_admin();
        if($view == 'data'){
            $this->data['scoreboard'] = $this->royalcanin_model->get_scoreboard($periode);
            $data['page'] = 1;
            $data['total'] = count($this->data['scoreboard']);
            $data['rows'] = $this->data['scoreboard'];
            header('Content-type: application/json');
            echo json_encode($data);
        }else{
            $this->data['content']  = $this->load->view('admin/listjawara', $this->data, true);
            $this->load->view('admin/theme_admin', $this->data);
        }
    }

    function clearance(){
        $this->session->sess_destroy();
        ?>
        <script>
            localStorage.removeItem('counter');
            localStorage.removeItem('limit_time');
        </script>
        <?php
        echo "All Session & Local Storage Clear !!!";
    }


}