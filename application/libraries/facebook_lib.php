<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
class Facebook_lib{
	var $CI;
	
	function Facebook_lib(){
        $this->CI =& get_instance();
    }
	
	function parse_signed_request($signed_request, $secret) {
		list($encoded_sig, $payload) = explode('.', $signed_request, 2);
		// decode the data
		$sig = $this->base64_url_decode($encoded_sig);
		$data = json_decode($this->base64_url_decode($payload), true);
		if (strtoupper($data['algorithm']) !== 'HMAC-SHA256') {
		  error_log('Unknown algorithm. Expected HMAC-SHA256');
		  return null;
		}
		// check sig
		$expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);
		if ($sig !== $expected_sig) {
		  error_log('Bad Signed JSON signature!');
		  return null;
		}
		return $data;
	}
	
	function base64_url_decode($input) {
		return base64_decode(strtr($input, '-_', '+/'));
	}
	
	function share_facebook($post){
		/*
		Contoh isi array $post
		$post['message']		= 'Check the story out on UrbanNotes@Urbanesia.com!!!';
		$post['link']			= 'http://www.urbanesia.com/article/urbansnote/2010/7/600/festival-komputer-indonesia-2010';
		$post['picture']		= 'http://static-10.urbanesia.com//img/article/2/0/article-20100715-article-aa12.promo_l-.ar_dv.jpg';
		$post['caption']		= 'www.urbanesia.com';
		$post['name']			= 'Festival Komputer Indonesia 2010';
		$post['description']	= 'It\'s coming back! Ingin mengulang kesuksesan pameran Festival Komputer Indonesia 2009, tahun ini pameran yang sama untuk ke-12 kalinya digelar kembali. Bertempat di Hall A, Hall B, dan Cendreawasih Hall - Jakarta Convention Center, Festival Komputer Indonesia 2010 dibuka sejak 14 Juli sampai 18 Juli 2010.';
*/
		$url = 'https://graph.facebook.com/'.$post['social_id'].'/feed';
		$parameter = array(
                        CURLOPT_HEADER=>0,
                        CURLOPT_SSL_VERIFYPEER=>FALSE,
                        CURLOPT_RETURNTRANSFER=>TRUE
                    );
		$this->CI->curl->create($url);
		$this->CI->curl->options($parameter);
		$this->CI->curl->post($post);
		
		$set_update = $this->CI->curl->execute();
		//var_dump($set_update);
                
		if(!$set_update) {
			return FALSE;
		}
		return $set_update;
	}
	
}