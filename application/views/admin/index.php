<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <h3 class="page-header">Daftar Peserta Kuis</h3>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <form class="form-inline" method="post">
                        <div class="form-group">
                            <select id="selectPeriode" class="form-control">
                                <option value="1">Periode I</option>
                                <option value="2">Periode II</option>
                                <option value="3">Periode III</option>
                                <option value="4">Periode IV</option>
                                <option value="5">Periode V</option>
                                <option value="6">Periode VI</option>
                            </select>
                            &nbsp;&nbsp;
                            <img id="loader" src="<?= base_url('static/img/loading.gif') ?>" alt="" style="display: none;">
                        </div>
                    </form>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered dataTables">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>[ UserID ] - Nama</th>
                                <th>Email</th>
                                <th>HP</th>
                                <th>Category</th>
                                <th>Skor</th>
                                <th>Total Skor</th>
                                <th>Point History</th>
                            </tr>
                            </thead>
                            <tbody id="list">
                            <?php $this->load->view('admin/loop_view')?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#selectPeriode').change(function(){
            var periode = $(this).val();
            $('#loader').show();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('admin/main/change_periode')?>",
                data: "periode="+periode,
                success: function(data){
                    $('#loader').hide();
                    $('.dataTables').dataTable().fnDestroy();
                    $('#list').empty();

                    $("#list").html(data);
                    $('.dataTables').dataTable({
                        "order": [[ 0, "asc" ]]
                    } );
                }
            });
        });
    });
</script>

