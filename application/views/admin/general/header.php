<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Royal Canin</title>
    <link href="<?= base_url()?>static/js/bootstrap-3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/admin/css/plugins/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?= base_url()?>static/css/admin.css" rel="stylesheet">
    <script type="text/javascript" src="<?= base_url()?>static/js/jquery-1.10.2.min.js"></script>

    <script type="text/javascript" src="<?= base_url()?>static/js/bootstrap-3.2.0/js/bootstrap.min.js"></script>
    <script src="<?= base_url()?>static/js/dataTables/jquery.dataTables.js"></script>
    <script src="<?= base_url()?>static/js/dataTables/dataTables.bootstrap.js"></script>
    <script>
        $(document).ready(function() {
            $('.dataTables').dataTable({
                "order": [[ 0, "asc" ]]
            } );
        });
    </script>
</head>
<body>

<nav class="navbar navbar-fixed-top navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Royal Canin</a>
        </div>

    </div>
</nav>