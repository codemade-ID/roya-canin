<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <h3 class="page-header">Point History : <strong class="text-danger"><?= @$users[0]->nama_lengkap?></strong></h3>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="<?= site_url('adminrc') ?>" class="btn btn-primary"> < Back to list</a>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered dataTables">
                            <thead>
                            <tr>
                                <th>Date Time</th>
                                <th>Question ID</th>
                                <th>User Answer</th>
                                <th>Periode</th>
                                <th>Point</th>
                                <th>Hint</th>
                            </tr>
                            </thead>
                            <tbody id="list">
                            <?php if(!empty($users)) : ?>
                                <?php foreach($users as $row) : ?>
                                    <tr>
                                        <td><?= date('d M Y H:i:s', strtotime($row->datetime))?></td>
                                        <td>
                                            <?php
                                            if($row->question_id == 1000) :
                                                echo "Share Timeline";
                                            elseif($row->question_id == 2000) :
                                                echo "Share Content";
                                            else :
                                                echo $row->question_id;
                                            endif;
                                            ?>
                                        </td>

                                        <td>
                                            <?php
                                            if($row->user_answer == 'f') :
                                                echo "Share Timeline";
                                            elseif($row->user_answer == 's') :
                                                echo "Share Content";
                                            elseif($row->user_answer == '') :
                                                echo "No Answer / Skip";
                                            else :
                                                echo $row->question_id;
                                            endif;
                                            ?>
                                        </td>
                                        <td><?= $row->periode?></td>
                                        <td><?= $row->point?></td>
                                        <td><?= $row->hint?></td>
                                    </tr>
                                <?php endforeach?>
                            <?php endif?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>