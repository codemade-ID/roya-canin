<?php if(!empty($users)) : ?>
    <?php $i=1; foreach($users as $row) : ?>
        <tr>
            <td><?= $i++?></td>
            <td>[ <?= $row->user_id?> ] - <?= $row->nama_lengkap?></td>
            <td><?= $row->email?></td>
            <td><?= $row->phone?></td>
            <td>
                <?php
                if($row->question_category == 1) :
                    echo "Anjing";
                else:
                    echo "Kucing";
                endif;
                ?>
            </td>
            <td><?= $row->poin?></td>
            <td>
                <?php if(!empty($total_skor)) : ?>
                    <?php foreach($total_skor as $row2) : ?>
                        <?php if($row->user_id == $row2->user_id && $row->session_id == $row2->session_id) :
                            echo $row2->total_poin;
                        endif?>
                    <?php endforeach;?>
                <?php endif;?>
            </td>
            <td class="text-center">
                <a href="<?= site_url('admin/main/history/'.$row->session_id)?>" class="btn btn-xs btn-info">lihat</a>
            </td>
        </tr>
    <?php endforeach?>
<?php endif?>
