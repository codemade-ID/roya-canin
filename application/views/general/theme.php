<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Royal Canin</title>
    <link type="text/css" rel="stylesheet" href="<?= base_url()?>static/fonts/fonts.css" />
    <link type="text/css" rel="stylesheet" href="<?= base_url()?>static/js/bootstrap-3.2.0/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?= base_url()?>static/css/royalcanin.css" />
    <script type="text/javascript" src="<?= base_url()?>static/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>static/js/bootstrap-3.2.0/js/bootstrap.min.js"></script>
    <!--<script>
        <?php /*$facebook_page_url = 'https://www.facebook.com/RoyalCanin.ID/?sk=app_493284184186480';*/?>
        var url_base = '<?php /*echo $facebook_page_url*/?>';
        if(parent.frames.length==0){
            open(url_base, '_top');
        }
    </script>-->
    <?php $register = $this->session->flashdata('register-success');?>
    <?php if($register) :?>
        <!-- Google Code for Royal Canin Photo Contest - 6 Nov Conversion Page -->
        <script type="text/javascript">
            /* <![CDATA[ */
            var google_conversion_id = 947418611;
            var google_conversion_language = "en";
            var google_conversion_format = "3";
            var google_conversion_color = "ffffff";
            var google_conversion_label = "7hpgCMnXi2EQ8-vhwwM";
            var google_remarketing_only = false;
            /* ]]> */
        </script>
        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
        <noscript>
            <div style="display:inline;">
                <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/947418611/?label=7hpgCMnXi2EQ8-vhwwM&amp;guid=ON&amp;script=0"/>
            </div>
        </noscript>

        <!-- Facebook Conversion Code for Royal Canin Photo Contest - 6 Nov -->

        <script>(function() {
                var _fbq = window._fbq || (window._fbq = []);
                if (!_fbq.loaded) {
                    var fbds = document.createElement('script');
                    fbds.async = true;
                    fbds.src = '//connect.facebook.net/en_US/fbds.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(fbds, s);
                    _fbq.loaded = true;
                }
            })();
            window._fbq = window._fbq || [];
            window._fbq.push(['track', '6033242087349', {'value':'0.00','currency':'USD'}]);
        </script>
        <noscript>
            <img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6033242087349&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" />
            </noscript>

            <!-- Twitter Conversion Code -->
        <script src="//platform.twitter.com/oct.js" type="text/javascript"></script>
        <script type="text/javascript">twttr.conversion.trackPid('ntrzw', { tw_sale_amount: 0,tw_order_quantity: 0 });</script>
        <noscript>
            <img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=ntrzw&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
            <img height="1" width="1" style="display:none;" alt="" src="//t.co/i/adsct?txn_id=ntrzw&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
        </noscript>
		<?php  endif; ?>
</head>
<body>
<?php
if(!$this->agent->is_mobile()){ ?>
<script>
    var facebook_page_url = 'https://www.facebook.com/RoyalCanin.ID/?sk=app_493284184186480';
    var url_base = facebook_page_url;
    if(parent.frames.length==0){
        open(url_base, '_top');
    }
</script>
<?php
}
?>

<?php if(isset($content)) : $this->load->view($content); endif; ?>

<?php $this->load->view('general/modal')?>

</body>
</html>