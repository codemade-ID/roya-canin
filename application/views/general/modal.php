<!--SYARAT & KETENTUAN-->
<div class="modal fade" id="SyaratKetentuan" tabindex="-1" role="dialog" aria-labelledby="SyaratKetentuan">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-apps">
            <h3>Syarat &amp; Ketentuan Facebook Apps Royal Canin</h3>
            <div class="table-bordered border-rc"></div>
            <div class="doubleenter"></div>
            <div class="syarat-ketentuan">
                <h4>Syarat Umum</h4>
                <ol>
                    <li>Periode Kuis: 7 - 13 December 2015</li>
                    <li>Kuis Royal Canin terbuka untuk siapa saja yang merupakan Warga Negara Indonesia (WNI) dan ber domisili di Indonesia dan memiliki kartu identitas yang masih berlaku.</li>
                    <li>Peserta kuis wajib untuk like Facebook Page Royal Canin Indonesia</li>
                    <li>Pemenang mingguan akan diumumkan melalui Facebook Page Royal Canin Indonesia setiap minggunya. </li>
                    <li>Pemenang utama akan diumumkan melalui Facebook Page Royal Canin Indonesia di akhir periode. </li>
                    <li>Pemenang mingguan hanya akan dapat menang satu kali. </li>
                    <li>Pemenang mingguan akan dipilih berdasarkan poin tertinggi. </li>
                    <li>Pemenang utama dipilih berdasarkan kombinasi poin tertinggi dan jumlah share terbanyak. </li>
                    <li>Di akhir permainan, peserta akan mendapatkan konten edukasi untuk menambah pengetahuan perawatan hewan peliharaan sesuai dengan skor akhir yang didapatkan. </li>
                    <li>Royal Canin Indonesia tidak akan memungut biaya apapun terkait dengan permainan ini. </li>
                    <li>Kuis Royal Canin tidak berlaku bagi seluruh karyawan Royal Canin dan karyawan agensi digital Royal Canin.</li>
                </ol>
                <div class="enter"></div>
                <h4>Tata Cara dan Syarat Umum Kuis</h4>
                <ol>
                    <li>Peserta hanya dapat mengikuti kuis maksimal 1 (satu) kali setiap minggunya.</li>
                    <li>Peserta wajib memilih salah satu jenis hewan peliharaan (anjing atau kucing) untuk pertanyaan kuis.</li>
                    <li>Peserta akan menjawab 10 (sepuluh) pertanyaan dari Royal Canin yang berkaitan tentang anjing atau kucing (sesuai dengan pilihan di nomor 2).</li>
                    <li>Setiap pertanyaan memiliki 4 (empat) pilihan jawaban. </li>
                    <li>Peserta hanya dapat memilih 1 (satu) jawaban di setiap pertanyaan. </li>
                    <li>Jawaban yang sudah dipilih tidak dapat diubah.</li>
                    <li>Setiap pertanyaan yang benar akan mendapatkan 10 (sepuluh) poin. </li>
                    <li>Tidak ada pengurangan poin untuk jawaban yang salah.</li>
                    <li>Setiap peserta memiliki masing-masing 3 (tiga) kesempatan dalam 1 (satu) kali permainan.</li>
                    <li>Peserta diberikan waktu 1 (satu) menit untuk menjawab semua pertanyaan (keterangan waktu terdapat di kanan atas).</li>
                    <li>Apabila peserta kehabisan waktu dan/atau kesempatan, maka permainan otomatis berakhir dan peserta akan langsung diarahkan ke halaman skor akhir. </li>
                    <li>Peserta dapat menggunakan petunjuk apabila tidak dapat menjawab pertanyaan (tombol petunjuk terdapat di kiri atas).</li>
                    <li>Setiap penggunaan petunjuk, maka peserta akan kehilangan satu kesempatan.</li>
                    <li>Peserta wajib memasukkan data diri berupa nama, email, dan nomor telepon untuk mengetahui skor akhir dari permainan. </li>
                    <li>Peserta bisa mendapatkan poin tambahan di akhir permainan dengan cara share konten yang sudah disediakan oleh Royal Canin Indonesia.</li>
                    <li>Setiap konten yang dishare akan menambahkan 1 (satu) poin. </li>
                    <li>Skor total merupakan kombinasi dari jumlah poin yang didapatkan dari jawaban yang benar dan poin tambahan dari share konten. </li>
                </ol>
            </div>
            <a href="javascript:;" class="close-x" data-dismiss="modal">X</a>
        </div>
    </div>
</div>

<!--PETUNJUK-->
<div class="modal fade" id="Petunjuk" tabindex="-1" role="dialog" aria-labelledby="Petunjuk">
    <div class="modal-dialog" role="document">
        <div id="hint" class="modal-content modal-petunjuk">

        </div>
    </div>
</div>

<!--STOP TIMER-->
<div class="modal fade" id="StopTimer" tabindex="-1" role="dialog" aria-labelledby="StopTimer">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-apps">
            <h1><img src="<?= base_url()?>static/img/timesup.png" /></h1>
            <div class="doubleenter"></div>
            <h3>Maaf waktu anda telah habis</h3>
            <div class="doubleenter"></div>
            <a href="<?= site_url('register?timer=empty')?>" class="btn-rc">Lihat skormu sekarang!</a>
        </div>
    </div>
</div>

<?php $alert = $this->session->flashdata('alert');?>
<?php if(!empty($alert)) :?>
<!--Alert-->
<div class="modal fade" id="alert" tabindex="-1" role="dialog" aria-labelledby="alert">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-apps">
            <h3>Alert</h3>
            <div class="doubleenter"></div>
            <p><?php echo $alert;?></p>
            <a href="javascript:;" class="close-x" data-dismiss="modal">X</a>
        </div>
    </div>
</div>
<script type="text/javascript">
$('#alert').modal('show');
</script>
<?php endif;?>

<!--FORM-->
<div class="modal fade" id="Register" tabindex="-1" role="dialog" aria-labelledby="Register">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-apps">
            <h1>Data Diri</h1>
            <form method="post" action="">
                <div class="row">
                    <div class="col-md-3 text-right"><label>Nama:</label></div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="text" name="nama_lengkap" class="form-control" required/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 text-right"><label>Email:</label></div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="email" name="email" class="form-control" required/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 text-right"><label>No. Telp:</label></div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="text" name="phone" class="form-control" required/>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn-rc">SUBMIT</button>
                </div>
            </form>
            <a href="javascript:;" class="close-x" data-dismiss="modal">X</a>
        </div>
    </div>
</div>


<div class="modal fade" id="video" tabindex="-1" role="dialog" aria-labelledby="SyaratKetentuan">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content modal-apps">
            <iframe width="100%" height="315" src="https://www.youtube.com/embed/dV_RU6rvra4" frameborder="0" allowfullscreen></iframe>
            <a href="javascript:;" class="close-x" data-dismiss="modal">X</a>
        </div>
    </div>
</div>