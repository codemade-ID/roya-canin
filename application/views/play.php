<script type="text/javascript" src="<?= base_url()?>static/js/jquery.countdown.min.js"></script>

<div class="container DINRoundPro">
    <div class="head-rc">
        <div class="logo-rc">
            <a href="<?= base_url() ?>">
                <img src="<?= base_url()?>static/img/logo-rc-red.png" />
            </a>
        </div>
        <div class="text-center">
            <div class="fourthenter"></div>
            <div class="row">
                <div class="col-md-4 text-left">
                    <div class="head-question help">
                        <a href="javascript:;" data-target="#Petunjuk" data-toggle="modal" data-id="<?= $detail_question->question_id?>">
                            <img src="<?= base_url()?>static/img/help.png" />
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="head-question life">
                        KESEMPATAN
                        <div class="enter"></div>
                        <?php
                        $total = 3;
                        $jumlah = $total_failed + $total_hint;
                        for($i=1; $i<= $total-$jumlah; $i++){?>
                            <img src="<?= base_url()?>static/img/paw.png" /> &nbsp;
                        <?php }?>

                    </div>
                </div>
                <div class="col-md-4 text-right">
                    <div class="head-question timer"><img src="<?= base_url()?>static/img/timer.png" />&nbsp;&nbsp;<span id="divCounter"></span></div>
                </div>
            </div>
            <div class="fourthenter"></div>
            <ul class="step-number">
                <?php if(!empty($question)): ?>
                    <?php $i=1; foreach($question as $row):
                        $sess = $this->session->userdata('q_'.$row->question_id);
                        if($i==$now_question){
                            $color = 'active';
                        }elseif(isset($sess)){
                            $color = $this->session->userdata('q_'.$row->question_id);
                        }
                        ?>
                        <li id="q<?= $i?>" class="<?= $color?>"><?=$i;?></li>
                    <?php $i++; endforeach;?>
                <?php endif;?>
            </ul>
            <div class="fourthenter"></div>

            <?php if(!empty($detail_question)) :?>
                <form class="pilihan" method="get">
                    <div class="row">
                        <div class="col-md-5">
                            <?php if($detail_question->question_img != null) :
                                if($detail_question->question_category == 1){ ?>
                                    <img src="<?= base_url('uploads/dog/'.str_replace(' ','',$detail_question->question_img))?>" alt="<?= $detail_question->question_img?>" class="table-bordered img-rounded">
                                <?php }else{?>
                                    <img src="<?= base_url('uploads/cat/'.str_replace(' ','',$detail_question->question_img))?>" alt="<?= $detail_question->question_img?>" class="table-bordered img-rounded">
                                <?php }
                                ?>
                            <?php endif?>
                        </div>
                        <div class="col-md-7">
                            <h4 class="text-left">
                                <?= $detail_question->question_title ?>
                            </h4>
                            <div id="option_a" class="option">
                                <input type="radio" id="radio1" name="answer" value="a" >
                                <label for="radio1"><span class="choice">A</span><?= $detail_question->option_a ?></label>
                                <div class="clearfix"></div>
                            </div>
                            <div id="option_b" class="option">
                                <input type="radio" id="radio2" name="answer" value="b">
                                <label for="radio2"><span class="choice">B</span><?= $detail_question->option_b ?></label>
                                <div class="clearfix"></div>
                            </div>
                            <div id="option_c" class="option">
                                <input type="radio" id="radio3" name="answer" value="c">
                                <label for="radio3"><span class="choice">C</span><?= $detail_question->option_c ?></label>
                                <div class="clearfix"></div>
                            </div>
                            <div id="option_d" class="option">
                                <input type="radio" id="radio4" name="answer" value="d">
                                <label for="radio4"><span class="choice">D</span><?= $detail_question->option_d ?></label>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="thirdenter"></div>
                    <?php if($now_question >= 10):?>
                        <a href="<?=site_url();?>register" class="btn-rc">Selanjutnya</a>
                    <?php else : ?>
                        <a href="<?=site_url();?>play/<?= $detail_question->question_category?>?q=<?=$now_question + 1;?>" class="btn-rc">Selanjutnya</a>
                    <?php endif; ?>
                </form>
            <?php endif;?>

            <div class="fourthenter"></div>
        </div>
    </div>
    <?php $this->load->view('general/footer')?>
</div>

<script>

    /* HTML5 LOCAL STORAGE */
    /* Ffungsi add, delete dan cek status local storage */
    function removeHtmlStorage(name) {
        localStorage.removeItem(name+'_time');
    }
    function setHtmlStorage(name,expires) {
        if (expires==undefined || expires=='null') { var expires = 3600; } // default: 1h
        var date = new Date();
        var schedule = Math.round((date.setSeconds(date.getSeconds()+expires))/1000);
        localStorage.setItem(name+'_time', schedule);
    }
    function statusHtmlStorage(name) {
        var date = new Date();
        var current = Math.round(+date/1000);

        // Get Schedule
        var stored_time = localStorage.getItem(name+'_time');
        if (stored_time==undefined || stored_time=='null') { var stored_time = 0; }

        // Expired
        if (stored_time < current) {
            removeHtmlStorage(name);
            return 0;
        } else {
            return 1;
        }
    }

    var interval;
    function timer(){
        var count = 60;
        if(localStorage.getItem("counter")){
            if(localStorage.getItem("counter") <= 0){
                var value = count;
            }else{
                var value = localStorage.getItem("counter");
            }
        }else{
            var value = count;
        }
        if (value < 10 && length.value != 2) value = '0' + value;
        document.getElementById('divCounter').innerHTML = '00:'+value;

        var counter = function (){
            if(value <= 0){
                localStorage.setItem("counter", count);
                value = count;
                $('#StopTimer').modal({
                    keyboard : false,
                    backdrop : 'static'
                });
                clearInterval(interval);
                setHtmlStorage('limit', 3600);
            }else{
                value -= 1;
                localStorage.setItem("counter", value);
            }
            if (value < 10 && length.value != 2) value = '0' + value;
            document.getElementById('divCounter').innerHTML = '00:'+value;
        };
        interval = setInterval(function(){
            counter();
        }, 1000);

    }

    function clear(){
        clearInterval(interval);
    }


    /*
     Cek status local storage :
     - Jika tedapat local storage 'counter_limit' (local storage 'counter_limit' belum expired)
     berarti user sudah pernah ikut dan kehabisan waktu.
     actionnya => redirect ke halaman register sebelum lihat skor yang dimiliki.

     - Jika tidak terdapat local storage 'counter_limit',
     artinya belum pernah jalanin game ini atau cache sudah lewat dari 24 jam. Cache local storage telah terhapus.
     actionnya => Set ulang cache localstorage dengan name 'counter_limit' dan timer game ter reset kembali.
     */

    var status = statusHtmlStorage('limit');
    console.log(statusHtmlStorage('limit'));
    if (status == 1) {
        window.location = "<?=base_url();?>register/?timer=empty";
    } else {
        timer();
    }
		
		
    $(document).ready(function(){

        $("input:radio").click(function(){
            if($(this).is(":checked")){
                var user_answer = $(this).val();
                var timer = $('.timer span span').html();
                $.ajax({
                    type: 'POST',
                    url: '<?= base_url()?>answer/',
                    datType: 'html',
                    data: {'user_answer':user_answer, 'question_id': <?=$detail_question->question_id;?>,'postsubmit' : 'true'},
                    timeout: 20000,
                    success: function(response){
                        if(response == "true"){
                            $('#option_'+user_answer).addClass("checked-true");
                            $("input:radio").attr("disabled", "disabled");
                            $('#q<?=$now_question;?>').removeClass('active');
                            $('#q<?=$now_question;?>').removeClass('false');
                            $('#q<?=$now_question;?>').addClass('true');
                        }else if(response == "false"){
                            $('#option_'+user_answer).addClass("checked-false");
                            $("input:radio").attr("disabled", "disabled");
                            $('#q<?=$now_question;?>').removeClass('active');
                            $('#q<?=$now_question;?>').removeClass('true');
                            $('#q<?=$now_question;?>').addClass('false');
                        }else{
                            //window.location = "<?=base_url();?>play/?q=<?=$now_question + 1;?>";
                        }
                    },
                    error: function(){
                        //window.location = "<?=base_url();?>play/?q=<?=$now_question + 1;?>";
                        alert("Error occured during Ajax request...");
                    }
                });
                return;
            }
        });

        $('#Petunjuk').on('show.bs.modal',function(event){
            clear();
            var target = $(event.relatedTarget);
            var question_id = target.data('id');
            $.ajax({
                type: "POST",
                url: "<?= base_url()?>hint",
                data: "question_id="+question_id,
                cache: false,
                success: function(data){
                    $('#hint').html(data);
                }
            });
            return true;
        });

        $('#Petunjuk').on('hide.bs.modal', function (e) {
            timer();
        })
    });
</script>