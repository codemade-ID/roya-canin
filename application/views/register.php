<div class="container DINRoundPro">
    <div class="head-rc">
        <div class="logo-rc">
            <a href="<?= base_url() ?>">
                <img src="<?= base_url()?>static/img/logo-rc-red.png" />
            </a>
        </div>
        <div class="text-center">
            <div class="fourthenter"></div>
            <h1>Terima kasih untuk partisipasimu dalam games ini!</h1>
            <div class="thirdenter"></div>
            <img src="<?= base_url()?>static/img/step-3/image-1.jpg" />
            <div class="thirdenter"></div>
            <a href="<?= site_url('result')?>" class="btn-rc">Ketahui Skormu Sekarang!</a>
            <div class="fourthenter"></div>
        </div>
    </div>
    <?php $this->load->view('general/footer')?>
</div>
<script>
    $(document).ready(function(){
        $('#Register').modal('show');
    })
</script>