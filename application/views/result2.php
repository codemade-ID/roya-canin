<div class="container DINRoundPro">
    <div class="head-rc">
        <div class="logo-rc">
            <a href="<?= base_url() ?>">
                <img src="<?= base_url()?>static/img/logo-rc-red.png" />
            </a>
        </div>
        <div class="text-center">
            <div class="fourthenter"></div>
            <h3>Total Skormu Sekarang :</h3>
            <div class="skor-rc"><?=$total_point;  ?></div>

            <div class="fourthenter"></div>
            <div class="row">
                <div class="col-md-6">
                    <img src="<?= base_url()?>static/img/step-6/image-4.jpg">
                    <div class="doubleenter"></div>
                    <div class="text-center">
                        <a class="btn-rc" href="<?= site_url('share')?>">Tambah Poinmu Di sini</a>
                    </div>
                </div>
		<div class="col-md-6">
                    <img src="<?= base_url()?>static/img/step-6/image-5.jpg">
                    <div class="doubleenter"></div>
                    <div class="text-center">
                        <a class="btn-rc fb_share" href="javascript:;">Share Skormu di Timeline</a>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <?php $this->load->view('general/footer')?>
</div>

<?php
$total = $total_point / 10;
$poin = floor($total) * 10;
?>

<div id="fb-root"></div>
<script src="https://connect.facebook.net/en_US/all.js"></script>
<script>
    $('.fb_share').click(function(){
        FB.init({
            appId  : '<?=$config_royalcanin['app_id']?>',
        });
        FB.ui(
            {
                method: 'feed',
                name: 'Let’s play, Pet Mates!',
                 link: '<?php echo base_url()?>',
                picture: '<?php echo base_url()?>static/img/share/<?= $poin?>.png',
                caption: 'http://royalcanin.co.id',
                description: 'Aku baru saja berhasil menjawab pertanyaan di games #UnderstandYourPets dari Royal Canin Indonesia. Yuk ikut main juga, ada kesempatan untuk menangkan hadiah setiap minggunya. Let’s play, Pet Mates!'
            },
            function(response) {
                if (response && response.post_id) {
                    $.ajax({
                        type: 'POST',
                        url: '<?= base_url()?>bonus_share',
                        datType: 'html',
                        data: {'user_answer':'f'},
                        success: function(response){
                            if(response == "success"){
                                window.location = "<?= base_url()?>result2";
                                return false;
                            }else{
                                alert("Tambahan poin hanya berlaku sehari sekali.");
                            }
                        },
                        error: function(){
                            alert("Error occured during Ajax request...");
                        }
                    });

                } else {
                    //alert('Post was not published.');
                }
            }
        );

    });

    $(document).ready(function(){
        $('#showVideo').click(function(){
            $('#video').modal('show');
        })
    })
</script>