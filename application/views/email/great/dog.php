<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="container" style="border:15px solid #e6e6e6;">
    <tr>
        <td>
            <table border="0" align="center" cellpadding="0" cellspacing="0" class="content">
                <tr>
                    <td colspan="3" align="center"><img src="https://apps.codemade.co.id/royalcanin/static/img/logo-rc-red.png" /></td>
                </tr>
                <tr>
                    <td colspan="3" align="center"><img src="https://apps.codemade.co.id/royalcanin/static/img/email/anjing-c2.jpg" /></td>
                </tr>
                <tr>
                    <td align="center"><a href="https://www.facebook.com/RoyalCanin.ID" target="_blank"><img src="https://apps.codemade.co.id/royalcanin/static/img/email-facebook.png" /></a></td>
                    <td><img src="https://apps.codemade.co.id/royalcanin/static/img/email-batas.png" /></td>
                    <td align="center"><a href="https://twitter.com/royalcanin_id" target="_blank"><img src="https://apps.codemade.co.id/royalcanin/static/img/email-twitter.png" /></a></td>
                </tr>
                <tr>
                    <td colspan="3" align="center"><h1>PAHAMI LEBIH DALAM KEBUTUHAN KHUSUS ANJINGMU</h1></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table  border="0" align="center" cellpadding="0" cellspacing="0" class="content"  style="background:#e6e6e7; padding:0;">
                <tr>
                    <td colspan="3" align="center"><img src="https://apps.codemade.co.id/royalcanin/static/img/email-content.png" width="100%" /></td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                        <h2>Hi Pet Mates</h2>
                        <p>Terima kasih sudah ikutan games <strong>#UnderstandYourPets!</strong> Wow! Kamu punya pengetahuan yang sudah baik tentang anjingmu, Pet Mates. Tapi, kamu bisa lho jadi Pet Mates yang lebih baik lagi untuk anjingmu.</p>
                        <p>Unduh infografik dan eBook dari kami yang berisi konten menarik mengenai anjingmu. Jangan lupa untuk main lagi ya minggu depan! Kalau skormu bisa lebih bagus dari sekarang, ada kejutan lain dari kami untukmu dan anjingmu.</p>
                        <p><br/><br/></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <table border="0" align="center" cellpadding="0" cellspacing="0" class="content">
                <tr>
                    <td style="width:33.66666%" align="center">
                        <a href="https://apps.codemade.co.id/royalcanin/download/RC_Dog_Infographic.png">
                            <img src="https://apps.codemade.co.id/royalcanin/static/img/email/on-infografik.png" width="80%" />
                        </a>
                    </td>
                    <td style="width:33.66666%" align="center">
                        <a href="https://apps.codemade.co.id/royalcanin/download/RC_Dog_eBook.pdf">
                            <img src="https://apps.codemade.co.id/royalcanin/static/img/email/on-ebook.png" width="80%" />
                        </a>
                    </td>

                    <td style="width:33.66666%" align="center"><img src="https://apps.codemade.co.id/royalcanin/static/img/email/off-musica.png" width="80%" /></td>
                </tr>
                <tr>
                    <td align="center"><img src="https://apps.codemade.co.id/royalcanin/static/img/email/anjing-l2.jpg" width="100%" /></td>
                    <td align="center"><a href="https://apps.codemade.co.id/royalcanin" target="_blank"><img src="https://apps.codemade.co.id/royalcanin/static/img/email/button-main.png" width="70%" /></a></td>
                    <td align="center"><img src="https://apps.codemade.co.id/royalcanin/static/img/email/anjing-r2.jpg" width="100%" /></td>
                </tr>
                <tr>
                    <td align="center"><a href="http://www.royalcanin.co.id/produk/products/produk-untuk-anjing/dimana-tempat-membeli-produk-royal-canin" target="_blank"><img src="https://apps.codemade.co.id/royalcanin/static/img/email/beli.png" width="80%" /></a></td>
                    <td align="center"><a href="http://www.royalcanin.co.id" target="_blank"><img src="https://apps.codemade.co.id/royalcanin/static/img/email/artikel.png" width="80%" /></a></td>
                    <td align="center"><a href="http://www.royalcanin.co.id/nutrisi-kesehatan/pendekatan-royal-canin/puzzle-nutrisi" target="_blank"><img src="https://apps.codemade.co.id/royalcanin/static/img/email/about.png" width="80%" /></a></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="color:#fff; background:#988a81; text-align:center; font-size:14px; padding:5px 0;" align="center">© 2015 Royal Canin. All Rights reserved </td>
    </tr>
</table>