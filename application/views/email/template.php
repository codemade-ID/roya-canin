<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Royal Canin</title>
    <style>
        body{ margin:0 auto; padding:0; font-family:'DINRoundPro', Arial, Helvetica, sans-serif; }
        img{ line-height:0; }
        table, tr, td{ border:none; margin:0; padding:0; }
        a{ outline:none; text-decoration:none; }
        @font-face {
            font-family: 'DINRoundPro';
            src: url('https://apps.codemade.co.id/royalcanin/static/fonts/DINRoundPro.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }
        @font-face {
            font-family: 'DINRoundPro-Light';
            src: url('https://apps.codemade.co.id/royalcanin/static/fonts/DINRoundPro-Light.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }
        @font-face {
            font-family: 'DINRoundPro-Medium';
            src: url('https://apps.codemade.co.id/royalcanin/static/fonts/DINRoundPro-Medium.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }
        @font-face {
            font-family: 'DINRoundPro-Bold';
            src: url('https://apps.codemade.co.id/royalcanin/static/fonts/DINRoundPro-Bold.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }
        @font-face {
            font-family: 'DINRoundPro-Black';
            src: url('https://apps.codemade.co.id/royalcanin/static/fonts/DINRoundPro-Black.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }
        table.container{ margin:0 auto; display:block; position:relative; }
        table.content{ text-align:center; padding:20px 0; display:inline-table; }
        h1{ color:#e12726; font-family:'DINRoundPro-Medium', Arial, Helvetica, sans-serif; margin-bottom:20px; font-size:36px; }
        h2{ color:#6d6e71; font-family:'DINRoundPro-Medium', Arial, Helvetica, sans-serif; margin-bottom:20px; font-size:28px; }
        p{ color:#6d6e71;  padding:0 20px; }
        table tbody{ width:570px; display:block; }
    </style>
</head>
<body>

<?php $this->load->view(@$msg_view)?>

</body>
</html>