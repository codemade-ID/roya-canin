<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="container" style="border:15px solid #e6e6e6;">
    <tr>
        <td>
            <table border="0" align="center" cellpadding="0" cellspacing="0" class="content">
                <tr>
                    <td colspan="3" align="center"><img src="https://apps.codemade.co.id/royalcanin/static/img/logo-rc-red.png" /></td>
                </tr>
                <tr>
                    <td colspan="3" align="center"><img src="https://apps.codemade.co.id/royalcanin/static/img/email/kucing-c3.jpg" /></td>
                </tr>
                <tr>
                    <td align="center"><a href="https://www.facebook.com/RoyalCanin.ID" target="_blank"><img src="https://apps.codemade.co.id/royalcanin/static/img/email-facebook.png" /></a></td>
                    <td><img src="https://apps.codemade.co.id/royalcanin/static/img/email-batas.png" /></td>
                    <td align="center"><a href="https://twitter.com/royalcanin_id" target="_blank"><img src="https://apps.codemade.co.id/royalcanin/static/img/email-twitter.png" /></a></td>
                </tr>
                <tr>
                    <td colspan="3" align="center"><h1>KASIH SAYANG TERBESAR ADALAH PERAWATAN DAN PERHATIAN TERBAIK</h1></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table  border="0" align="center" cellpadding="0" cellspacing="0" class="content"  style="background:#e6e6e7; padding:0;">
                <tr>
                    <td colspan="3" align="center"><img src="https://apps.codemade.co.id/royalcanin/static/img/email-content.png" width="100%" /></td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                        <h2>Hi Pet Mates</h2>
                        <p>Terima kasih sudah ikutan games <strong>#UnderstandYourPets!</strong> Luar biasa! Kamu pasti akrab dan sangat memahami kucingmu, Pet Mates. Tingkatkan terus pengetahuanmu tentang kucingmu supaya kamu selalu bisa memberikan yang terbaik untuk mereka.</p>
                        <p>Karena kamu Pet Mates luar biasa, kami memberikan kejuatan lengkap untukmu dan kucingmu. Yuk unduk infografik, eBook dan musik eksklusif untuk relaksasi kucingmu disini. Jangan lupa berbagi informasi tentang kucing ke Pet Mates lain ya supaya semua Pet Mates selalu bisa memberikan yang terbaik untuk kucingnya</p>
                        <p><br/><br/></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <table border="0" align="center" cellpadding="0" cellspacing="0" class="content">
                <tr>
                    <td style="width:33.66666%" align="center">
                        <a href="https://apps.codemade.co.id/royalcanin/download/RC_Cat_Infographic.png">
                            <img src="https://apps.codemade.co.id/royalcanin/static/img/email/on-infografik.png" width="80%" />
                        </a>
                    </td>
                    <td style="width:33.66666%" align="center">
                        <a href="https://apps.codemade.co.id/royalcanin/download/RC_Cat_eBook.pdf">
                            <img src="https://apps.codemade.co.id/royalcanin/static/img/email/on-ebook.png" width="80%" />
                        </a>
                    </td>
                    <td style="width:33.66666%" align="center">
                        <a href="https://apps.codemade.co.id/royalcanin/download/RC_Song_for_Pets.mp3">
                            <img src="https://apps.codemade.co.id/royalcanin/static/img/email/on-music.png" width="80%" />
                        </a>
                    </td>

                </tr>
                <tr>
                    <td align="center"><img src="https://apps.codemade.co.id/royalcanin/static/img/email/kucing-l3.jpg" width="100%" /></td>
                    <td align="center"><a href="https://apps.codemade.co.id/royalcanin" target="_blank"><img src="https://apps.codemade.co.id/royalcanin/static/img/email/button-main.png" width="70%" /></a></td>
                    <td align="center"><img src="https://apps.codemade.co.id/royalcanin/static/img/email/kucing-r3.jpg" width="100%" /></td>
                </tr>
                <tr>
                    <td align="center"><a href="http://www.royalcanin.co.id/produk/products/produk-untuk-anjing/dimana-tempat-membeli-produk-royal-canin" target="_blank"><img src="https://apps.codemade.co.id/royalcanin/static/img/email/beli.png" width="80%" /></a></td>
                    <td align="center"><a href="http://www.royalcanin.co.id" target="_blank"><img src="https://apps.codemade.co.id/royalcanin/static/img/email/artikel.png" width="80%" /></a></td>
                    <td align="center"><a href="http://www.royalcanin.co.id/nutrisi-kesehatan/pendekatan-royal-canin/puzzle-nutrisi" target="_blank"><img src="https://apps.codemade.co.id/royalcanin/static/img/email/about.png" width="80%" /></a></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="color:#fff; background:#988a81; text-align:center; font-size:14px; padding:5px 0;" align="center">© 2015 Royal Canin. All Rights reserved </td>
    </tr>
</table>