<div class="container DINRoundPro">
    <div class="head-rc">
        <div class="logo-rc">
            <a href="<?= base_url() ?>">
                <img src="<?= base_url()?>static/img/logo-rc-red.png" />
            </a>
        </div>
        <div class="text-center">
            <div class="doubleenter"></div>
            <img src="<?= base_url()?>static/img/step-1/image-1.jpg" />
            <h1>Sudahkah Kamu Mengenal <br/> Anjing atau Kucingmu?</h1>
            <p>Meskipun sudah lama memelihara anjing dan kucing, kebanyakan pemilik ternyata masih tidak mengenali anjing atau kucing mereka secara baik. Buktikan kalau kamu Pet Mates sejati! Uji kemampuan seberapa kamu mengenal anjing atau kucingmu dalam mini games ini. Akan ada hadiah menarik setiap minggunya. Melalui mini games ini kamu juga bisa menambah pengetahuan lebih dalam tentang anjing dan kucingmu!</p>
            <div class="doubleenter"></div>
            <h3>Let's play, Pet Mates!</h3>
            <div class="doubleenter"></div>
            <a href="<?= site_url('choice')?>" class="btn-rc">MULAI</a>
            <div class="doubleenter"></div>
            <a href="javascript:;" class="rc-sk" data-target="#SyaratKetentuan" data-toggle="modal">Syarat &amp; Ketentuan</a>
        </div>
    </div>
    <?php $this->load->view('general/footer')?>
</div>