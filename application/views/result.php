<div class="container DINRoundPro">
    <div class="head-rc">
        <div class="logo-rc">
            <a href="<?= base_url() ?>">
                <img src="<?= base_url()?>static/img/logo-rc-red.png" />
            </a>
        </div>
        <div class="text-center">
            <div class="fourthenter"></div>
            <h3>Skormu:</h3>
            <div class="skor-rc"><?=$total_point;  ?></div>
            <div class="thirdenter"></div>
            <?php if($total_point >= 0 && $total_point <= 40) :?>
                <h1>"You are a Good Pet Mates"</h1>
                <p>
                    Kamu Pet Mates yang baik! Tapi baik saja tidak cukup, pets-mu butuh mendapatkan perawatan yang
                    terbaik dan perawatan yang baik datang dari pengetahuan yang mendalam tentang pets-mu.
                    Tapi, jangan khawatir, kamu bisa jadi Pet Mates yang lebih baik lagi dengan infografik yang
                    sudah kami kirimkan ke email-mu. Ayo jadi pet mates yang lebih baik lagi dan uji lagi kemampuanmu.
                </p>
            <?php elseif($total_point >= 50 && $total_point <= 70):?>
                <h1>"You are a Great Pet Mates"</h1>
                <p>
                    Kamu Pet Mates yang sangat baik! Pengetahuanmu tentang pets-mu sudah cukup luas dan kamu pasti sudah
                    merawat mereka dengan cukup baik. Tapi, kamu bisa jadi Pet Mates yang lebih baik lagi,
                    karena itu kami mengirimkan infografik dan eBook ke email kamu supaya kamu bisa memberikan perawatan
                    yang lebih baik lagi untuk pets-mu. Ayo jadi Super Pet Mates dan dapatkan skor lebih baik lagi!
                </p>
            <?php elseif($total_point >= 80 && $total_point <= 100):?>
                <h1>"You are a Super Pet Mates"</h1>
                <p>
                    Kamu Pet Mates yang luar biasa! Kamu sudah mengenal pets-mu luar dalam dan mengetahui semua kebutuhan
                    mereka yang harus dipenuhi oleh seorang Pet Mates. Terus berikan perawatan terbaik untuk pets-mu ya.
                    Sebagai hadiah karena kamu sudah menjadi Pet Mates yang luar biasa kami mengirimkan infografik, eBook,
                    dan musik eksklusif untuk pets-mu ke email. Keep up the good work!
                </p>
            <?php endif;?>
            <div class="fourthenter"></div>
            <div class="row">
                <div class="col-md-6">
                    <img src="<?= base_url()?>static/img/step-6/image-2.jpg">
                    <div class="doubleenter"></div>
                    <div class="text-center">
                        <a class="btn-rc" href="<?= site_url('share')?>">Tambah Poinmu Di sini</a>
                    </div>
                </div>
                <?php
                parse_str( parse_url('https://www.youtube.com/watch?v=dV_RU6rvra4', PHP_URL_QUERY ), $my_array_of_vars );
                $youtube_id = $my_array_of_vars['v'];
                ?>
                <div class="col-md-6">
                    <a href="javascript:;" id="showVideo">
                        <img src="http://i.ytimg.com/vi/<?php echo $youtube_id;?>/hqdefault.jpg" width="269" height="255"/>
                    </a>
                    <div class="doubleenter"></div>
                    <div class="text-center">
                        <a class="btn-rc fb_share" href="javascript:;">Share Skormu di Timeline</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('general/footer')?>
</div>

<?php
$total = $total_point / 10;
$poin = floor($total) * 10;
?>
<div id="fb-root"></div>
<script src="https://connect.facebook.net/en_US/all.js"></script>
<script>
    $('.fb_share').click(function(){
        FB.init({
            appId  : '<?=$config_royalcanin['app_id']?>',
        });
        FB.ui(
            {
                method: 'feed',
                name: 'Let’s play, Pet Mates!',
                link: '<?php echo base_url()?>',
                picture: '<?php echo base_url()?>static/img/share/<?= $poin?>.png',
                caption: 'http://royalcanin.co.id',
                description: 'Aku baru saja berhasil menjawab pertanyaan di games #UnderstandYourPets dari Royal Canin Indonesia. Yuk ikut main juga, ada kesempatan untuk menangkan hadiah setiap minggunya. Let’s play, Pet Mates!'
            },
            function(response) {
                if (response && response.post_id) {
                    $.ajax({
                        type: 'POST',
                        url: '<?= base_url()?>bonus_share',
                        datType: 'html',
                        data: {'user_answer':'f'},
                        success: function(response){
                            if(response == "success"){
                                window.location = "<?= base_url()?>result2";
                                return false;
                            }else{
                                alert("Tambahan poin hanya berlaku sehari sekali.");
                            }
                        },
                        error: function(){
                            alert("Error occured during Ajax request...");
                        }
                    });

                } else {
                    //alert('Post was not published.');
                }
            }
        );

    });
    $(document).ready(function(){
        $('#showVideo').click(function(){
            $('#video').modal('show');
        })
    })
</script>