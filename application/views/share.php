<div class="container DINRoundPro">
    <div class="head-rc">
        <div class="logo-rc">
            <a href="<?= base_url() ?>">
                <img src="<?= base_url()?>static/img/logo-rc-red.png" />
            </a>
        </div>
        <div class="text-center">
            <div class="fourthenter"></div>
            <h1>Share Konten dan Tambah Poinmu!</h1>
            <div class="thirdenter"></div>
            <?php if(!empty($share_item)) : ?>
                <?php foreach($share_item as $i => $row) :
                    if($i%2 == 0 ): ?>
                        <div class="row">
                    <?php endif;?>

                    <div id="box<?= $row->share_id?>" class="col-md-6" data-id="<?= $row->share_id?>">
                        <img src="<?= $row->share_image?>" class="img-rounded table-bordered" width="253" height="203"/>
                        <a href="javascript:;" data-href="<?= str_replace(strstr($row->share_url,'&src='),'',$row->share_url)?>" class="share-sosmed">
                            <img src="<?= base_url()?>static/img/facebook-share.png" />
                        </a>
                    </div>

                    <?php if($i%2 == 1) : ?>
                        </div>
                        <div class="fourthenter"></div>
                <?php endif;?>
                <?php endforeach;?>
            <?php endif;?>

            <a class="btn-rc" href="<?= base_url()?>result2">Lihat Total Skormu</a>
        </div>
    </div>
    <?php $this->load->view('general/footer')?>
</div>

<div id="fb-root"></div>
<script src="https://connect.facebook.net/en_US/all.js"></script>
<script>
    $(document).ready(function(){
        $('.share-sosmed').click(function(){
            var id = $(this).parent().data('id');
            var target = $(this).data('href');

            FB.init({
                appId  : '<?=$config_royalcanin['app_id']?>',
            });
            FB.ui(
                {
                    method: 'feed',
                    name: 'Lets play, Pet Mates! Royal Canin',
                    link: target
                },
                function(response) {
                    if (response && response.post_id) {
                        $.ajax({
                            type: 'POST',
                            url: '<?= base_url()?>bonus_share',
                            datType: 'html',
                            data: {'user_answer':'s','share_id':id},
                            success: function(response){
                                if(response == "success"){
                                    $('div#box'+id).fadeOut();
                                    window.location = "";
                                    return false;
                                }else{
                                    alert("Tambahan poin hanya berlaku sehari sekali.");
                                }
                            },
                            error: function(){
                                alert("Error occured during Ajax request...");
                            }
                        });
                    } else {
                        $('div#box'+id).fadeIn();
                    }
                }
            );
        });
    })
</script>