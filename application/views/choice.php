<div class="container DINRoundPro">
    <div class="head-rc">
        <div class="logo-rc">
            <a href="<?= base_url() ?>">
                <img src="<?= base_url()?>static/img/logo-rc-red.png" />
            </a>
        </div>
        <div class="text-center">
            <div class="fourthenter"></div>
            <h1>Pilih Jenis Peliharaanmu Di Sini</h1>
            <div class="thirdenter"></div>
            <div class="row">
                <div class="col-md-6">
                    <img src="<?= base_url()?>static/img/step-2/image-1.jpg" class="img-rounded table-bordered" />
                    <div class="doubleenter"></div>
                    <div class="text-center">
                        <a href="<?= site_url('play/1')?>" class="btn-rc">ANJING</a>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="<?= base_url()?>static/img/step-2/image-2.jpg" class="img-rounded table-bordered" />
                    <div class="doubleenter"></div>
                    <div class="text-center">
                        <a href="<?= site_url('play/2')?>" class="btn-rc">KUCING</a>
                    </div>
                </div>
            </div>
            <div class="fourthenter"></div>
        </div>
    </div>
    <?php $this->load->view('general/footer')?>
</div>