-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 18, 2015 at 09:51 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `royalcanin`
--

-- --------------------------------------------------------

--
-- Table structure for table `royalcanin__answer`
--

CREATE TABLE IF NOT EXISTS `royalcanin__answer` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `periode` int(1) NOT NULL,
  `datetime` datetime NOT NULL,
  `user_id` int(10) NOT NULL,
  `question_id` int(10) NOT NULL,
  `user_answer` varchar(1) NOT NULL,
  `timer` decimal(10,0) NOT NULL,
  `point` decimal(10,0) NOT NULL,
  `status_answer` varchar(15) DEFAULT NULL,
  `hint` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=6 ;

--
-- Dumping data for table `royalcanin__answer`
--

INSERT INTO `royalcanin__answer` (`id`, `periode`, `datetime`, `user_id`, `question_id`, `user_answer`, `timer`, `point`, `status_answer`, `hint`) VALUES
(1, 1, '2015-10-18 09:45:43', 1, 37, 'a', '0', '10', 'true', 'no'),
(2, 1, '2015-10-18 09:45:49', 1, 32, 'a', '0', '10', 'true', 'yes'),
(3, 1, '2015-10-18 09:45:52', 1, 35, 'b', '0', '0', 'false', 'no'),
(4, 1, '2015-10-18 09:45:55', 1, 33, 'a', '0', '10', 'true', 'no'),
(5, 1, '2015-10-18 09:45:57', 1, 49, 'd', '0', '0', 'false', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `royalcanin__question`
--

CREATE TABLE IF NOT EXISTS `royalcanin__question` (
  `question_id` int(11) NOT NULL AUTO_INCREMENT,
  `question_title` text,
  `option_a` varchar(225) DEFAULT NULL,
  `option_b` varchar(225) DEFAULT NULL,
  `option_c` varchar(225) DEFAULT NULL,
  `option_d` varchar(225) DEFAULT NULL,
  `question_hint` text,
  `question_img` varchar(225) DEFAULT NULL,
  `question_answer` varchar(1) DEFAULT 'a',
  `question_category` int(1) DEFAULT NULL,
  `question_periode` int(1) DEFAULT NULL,
  PRIMARY KEY (`question_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=51 ;

--
-- Dumping data for table `royalcanin__question`
--

INSERT INTO `royalcanin__question` (`question_id`, `question_title`, `option_a`, `option_b`, `option_c`, `option_d`, `question_hint`, `question_img`, `question_answer`, `question_category`, `question_periode`) VALUES
(1, 'Seberapa sering kucing makan dalam satu hari?', '2 - 3 kali sehari', '10 - 12 kali sehari', '12 - 18 kali sehari', '6 - 10 kali sehari', 'Kucing bisa makan 12 ? 18 kali sehari, dengan waktu kurang dari 30 menit selama 24 jam. Kucing hanya mengunyah beberapa gram dari makanannya setiap mereka makan. ', 'RC_Cat_ Week1_01.png', 'a', 2, 1),
(2, 'Sesuai dengan jenis makanan dan nutrisi yang dibutuhkan oleh kucing, kucing merupakan jenis hewan?', 'Omnivor', 'Herbivor', 'Karnivor', 'Ambivor', 'Kucing adalah hewan karnivora, dan tubuh mereka teradaptasi untuk diet karnivor. Memberikan mereka makan dengan nutrisi kesehatan yang sesuai dengan umur, ukuran, gaya hidup, dan kebutuhan khusus yang dimiliki oleh kucing dapat berkontribusi untuk menjaga kesehatannya', 'RC_Cat_ Week1_02.png', 'a', 2, 1),
(3, 'Di antara ras-ras kucing di bawah, manakah yang paling rentan mengalami masalah hairball dikarenakan rambut mereka yang panjang?', 'Maine Coon', 'Bengal', 'Persia', 'Turkish Angora', 'Karena rambutnya yang panjang dan tebal, kucing Persia secara rutin menelan rambutnya sendiri saat sedang self-grooming. ', 'RC_Cat_ Week1_03.png', 'a', 2, 1),
(4, 'Di antara karakter kesehatan di bawah ini, manakah yang bukan karakter kesehatan dari Kucing Persia?', 'Kesulitan mengambil kibble akibat rahang brachycephalic', 'Beresiko terhadap hairball', 'Sistem pencernaan sensitif', 'Fase pertumbuhan panjang', 'Karakter kesehatan kucing Persia termasuk gampang terkena hairball karena rambutnya yang panjang, lalu sistem pencernaan yang sensitif, dan rahang brachychepalic (yang memiliki arti pendek di Bahasa Yunani). ', 'RC_Cat_ Week1_04.png', 'a', 2, 1),
(5, 'Gigi susu anak kucing akan muncul pada usia antara?', '1 - 2 minggu', '6 - 8 minggu', '10 - 12 minggu', '2 - 4 minggu', 'Gigi susu anak kucing akan mulai muncul di antara umur 2 hingga 4 minggu. ', 'RC_Cat_ Week1_05.png', 'a', 2, 1),
(6, 'Manakah di antara pilihan di bawah ini yang bukan merupakan efek jangka panjang dari obesitas di kucing?', 'Diabetes', 'Tekanan darah tinggi', 'Melemahnya fungsi sistem imun', 'Rabies', 'Kucing yang mengalami kelebihan berat badan atau obesitas berpotensi untuk terkena serangan diabetes, tekanan darah tinggi dan juga sistem imun yang melemah.', 'RC_Cat_ Week1_06.png', 'a', 2, 1),
(7, 'Di antara ras-ras kucing di bawah ini, manakah yang mengalami late maturity atau baru dewasa sepenuhnya di umur 2 - 3 tahun?', 'British Shorthair', 'Maine Coon', 'Persia', 'Bengal', 'British Shorthair mempunyai karakter unik late maturity di mana mereka baru mencapai ukuran dewasa sepenuhnya di umur 2 ? 3 tahun. ', 'RC_Cat_ Week1_07.png', 'a', 2, 1),
(8, 'Berapakah suhu air yang tepat untuk memandikan kucingmu?', '360 - 370 C', '300 - 320 C', '340 - 350 C', '380 - 390 C', 'Kucing harus dimandikan pada suhu yang tepat, tidak terlalu panas dan tidak terlalu dingin, yaitu berkisar pada suhu 360 - 370 C', 'RC_Cat_ Week1_08.png', 'a', 2, 1),
(9, 'Dibulan keberapakah gigi permanen dari anak kucing akan mulai muncul?', '8', '5', '4', '9', 'Pada umur 4 bulan. gigi susu mulai lepas dan digantikan gigi permanen. Pada masa ini kadang-kadang anak kucing  makan lebih sedikit karena ada rasa sakit di gusi. Semua gigi susu biasanya sudah digantikan oleh gigi permanen pada umur 6 bulan. pada umur 7 bulan biasanya sudah lengkap semua gigi permanen sebanyak 30 buah', 'RC_Cat_ Week1_09.png', 'a', 2, 1),
(10, 'Berapakah berat badan ideal untuk Kucing Persia jantan?', '3 - 5 kg', '5 - 7 kg', '8 - 10 kg', '2 - 3 kg', 'Berat badan ideal dari Kucing Persia jantan berkisar antara 5 ? 7 kg di mana Kucing Persia tidak malnutrisi ataupun kelebihan nutrisi sehingga menemui bahaya obesitas. ', 'RC_Cat_ Week1_10.png', 'a', 2, 1),
(11, 'Di antara rasa-rasa di bawah ini, manakah rasa yang tidak bisa dideteksi oleh indra perasa kucing?', 'Asin', 'Pahit', 'Manis', 'Asam', 'Kucing dapat merasakan rasa-rasa seperti asin, pahit, ataupun asam dalam makanannya. Namun, kucing tidak dapat mendeteksi rasa manis. ', 'RC_Cat_ Week1_11.png', 'a', 2, 1),
(12, 'Temperatur normal dari tubuh kucing adalah? ', '380 - 38,50 C', '300 - 310 C', '340 - 350 C', '390 - 400 C', 'Suhu tubuh atau temperature normal dari kucing berkisar antara 380 - 38,50 C', 'RC_Cat_ Week1_12.png', 'a', 2, 1),
(13, 'Sekitar berapa persen waktu terjaga dari kucing yang mereka habiskan untuk menjilati tubuh mereka sendiri?', '30%', '50%', '25%', '40%', 'Kucing menghabiskan 30% dari waktu 24 jam untuk grooming atau menjilati tubuh mereka sendiri. ', 'RC_Cat_ Week1_13.png', 'a', 2, 1),
(14, 'Apakah tanda-tanda dari radang sendi pada kucing yang sudah berumur?', 'Banyak aktivitas dan terlihat gelisah', 'Membersihkan bagian genital secara terus-menerus', 'Sebagian besar waktu dihabiskan untuk tidur', 'Terlihat ragu-ragu ketika harus turun dari tempat tinggi', 'Kucing yang sudah senior atau berumur, sering mengalami radang sendi, gejala ini dapat terlihat dari kelakuan sehari-hari mereka di mana mereka akan sering terlihat ragu-ragu ketika harus turun dari tempat tinggi.', 'RC_Cat_ Week1_14.png', 'a', 2, 1),
(15, 'Darimanakah asal kucing Siam?', 'Thailand', 'Indonesia', 'Bangladesh', 'Vietnam', 'Kucing Siam adalah salah satu ras kucing pertama yang diakui jelas sebagai kucing berjenis oriental. Sesuai dengan namanya, Siam berasal dari Negara Siam yang sekarang bernama Thailand. ', 'RC_Cat_ Week1_15.png', 'a', 2, 1),
(16, 'Di antara ras kucing berikut mana yang mempunyai motif khas belang - belang?', 'Bengal', 'Persia', 'Maine Coon', 'Persia', 'Kucing Bengal yang merupakan persilangan antara Kucing American Shorthair dan Kucing Leopard memiliki motif khas belang ? belang dengan dominasi warna hitam dan coklat. ', 'RC_Cat_ Week1_16.png', 'a', 2, 1),
(17, 'Mana di antara makanan berikut yang tidak boleh kamu berikan pada kucingmu? ', 'Daging ayam', 'Ikan', 'Hati', 'Nasi', 'Kucing adalah hewan semi karnivora, dan makanan yang mereka makan tidak sama dengan manusia yang omnivora. Karena itu jangan memberikan nasi pada kucing karena nasi adalah makanan manusia', 'RC_Cat_ Week1_17.png', 'a', 2, 1),
(18, 'Manakah cara yang benar dalam menyisir rambut kucing?', 'Sisir pertama searah tumbuhnya rambut dan kemudian ke arah sebaliknya', 'Sisir pertama dari arah ekor ke kepala', 'Sisir pertama dari arah mana saja yang terlihat paling kusut', 'Sisir pertama dari arah kepala ke ekor', 'Cara yang benar dalam menyisir rambut kucing adalah menyisir searah tumbuhnya rambut dan kemudian sisirlah kearah sebaliknya. ', 'RC_Cat_ Week1_18.png', 'a', 2, 1),
(19, 'Sisir seperti apakah yang harus kamu gunakan apabila kucingmu memiliki rambut yang panjang?', 'Sisir sikat ?bergigi rapat?', 'Sisir kayu ?bergigi renggang?', 'Sisir plastik ?bergigi rapat?', 'sisir logam ?bergigi renggang?', 'Untuk kucing dengan rambut panjang, gunakanlah sisir logam bergigi renggang yang dapat menyisir rambut yang kusut dan panjang. ', 'RC_Cat_ Week1_19.png', 'a', 2, 1),
(20, 'Untuk ras kucing berambut pendek berapa banyak dalam seminggu kamu harus menyisir mereka?', 'Sekali', 'Dua kali', 'Tiga kali', 'Tidak perlu ', 'Menyisir rambut kucing sangat perlu untuk menghindari rambut yang menggumpal dan mengurangi resiko hairball dari rambut-rambut mudah rontok yang akan ditelan oleh kucing saat mereka self-grooming. Untuk kucing berambut pendek, cukup disisir paling tidak sekali seminggu.', 'RC_Cat_ Week1_20.png', 'a', 2, 1),
(21, 'Daerah mana yang harus dihindari ketika kamu menggunakan sampo saat memandikan kucingmu?', 'Perut', 'Punggung', 'Ekor', 'Kepala', 'Hindari daerah kepala saat memandikan kucing karena sampo yang berbahan kimia dapat membahayakan daerah kepala kucing. ', 'RC_Cat_ Week1_21.png', 'a', 2, 1),
(22, 'Manakah faktor paling penting bagi kucing dalam memilih makanan yang mereka sukai?', 'Rasa dan Warna', 'Porsi', 'Bentuk', 'Aroma dan Tekstur', 'Kucing tidak mementingkan rasa ataupun bentuk dari makanan, yang paling penting dari kucing untuk memilih makanan yang mereka sukai adalah bau dan tekstur. ', 'RC_Cat_ Week1_22.png', 'a', 2, 1),
(23, 'Di antara ras-ras kucing di bawah ini, manakah kucing yang tidak memiliki rambut sama sekali?', 'Maine Coon', 'Persia', 'Sphynx', 'Turkish Angora', 'Sphynx adalah kucing yang unik. Berbeda dengan kucing lainnya, kucing Sphynx tidak memiliki rambut sama sekali. ', 'RC_Cat_ Week1_23.png', 'a', 2, 1),
(24, 'Di antara ras-ras kucing di bawah ini, manakah kucing yang merupakan ras terbesar di antara ras kucing lain?', 'Bengal', 'Norwegian Forest', 'Ragdoll', 'Maine Coon', 'Kucing Maine Coon adalah kucing ras murni terbesar, berat badan dari kucing Maine Coon dewasa bisa mencapai 10 kg. ', 'RC_Cat_ Week1_24.png', 'a', 2, 1),
(25, 'Manakah yang bukan cara kucing menandai teritorinya?', 'Mengeluskan badannya', 'Mencakar cakar suatu benda', 'Menyemprotkan feromon', 'Mengendus tempat tersebut', 'Kucing menandai  teritori-nya dengan cara mengeluskan badannya ke barang atau pemiliknya, mereka juga melakukannya dengan mencakar cakar suatu benda, atau menyemprotkan feromon.', 'RC_Cat_ Week1_25.png', 'a', 2, 1),
(26, 'Diantara ras-ras anjing berikut, mana ras yang memiliki rambut panjang?', 'Basset Hound', 'Jack Russel', 'Dalmatian', 'Shih Tzu', 'Shih Tzu adalah salah satu ras anjing tertua yang berasal dari Tiongkok. Anjing ras ini memiliki bulu yang panjang dengan berbagai warna dan pola pada bulunya. Bulunya yang panjang dapat menutupi telinga, badan, moncong, hingga kaki.', 'RC_Dog_Week1_01.jpg', 'a', 1, 1),
(27, 'Di bawah ini, mana yang merupakan karakter kesehatan anjing yang lebih sering tinggal di dalam rumah (indoor)?', 'Mudah stress', 'Kelebihan berat badan', 'Pilih-pilih makanan', 'Periode pertumbuhan cepat', 'Anjing indoor menghabiskan banyak waktu untuk tidur dan makan sehingga mereka jarang beraktivitas dan berolahraga. Hal ini bisa menyebabkan kelebihan berat badan atau obesitas.', 'RC_Dog_Week1_02.jpg', 'a', 1, 1),
(28, 'Apa keahlian anjing Beagle?', 'Menggembala hewan ternak', 'Penciuman tajam dan melacak', 'Berenang', 'Berlari', 'Beagle adalah anjing pemburu dan berasal dari Inggris. Dalam sejarahnya, anjing ini sering dimanfaatkan dalam berburu kelinci karena memiliki penciuman yang tajam.', 'RC_Dog_Week1_03.jpg', 'a', 1, 1),
(29, 'Apa yang harus kamu lakukan jika anjingmu lebih sering tinggal di dalam ruangan?', 'Memantau berat badannya secara rutin', 'Menyediakan makanan dengan kandungan energi ting', 'Mengganti makanannya secara rutin', 'Memberikan camilan', 'Anjing yang tinggal di dalam ruangan rentan terhadap obesitas sehingga kamu harus memantau berat badannya secara rutin.', 'RC_Dog_Week1_04.jpg', 'a', 1, 1),
(30, 'Berdasarkan makanan dan nutrisi yang dibutuhkan anjing, anjing adalah hewan?', 'Ambivora', 'Herbivora', 'Semi Karnivora', 'Omnivora', 'Berdasarkan makanan dan nutrisi yang dibutuhkan oleh anjing, anjing adalah hewan karnivora dan tidak seharunya diberikan makanan manusia karena manusia adalah omnivora', 'RC_Dog_Week1_05.jpg', 'a', 1, 1),
(31, 'Berapa rata-rata berat badan anak anjing Great Dane ketika baru lahir?', '100 ? 150 gr', '350 ? 450 gr', '600 ? 700 gr', '850 ? 950 gr', 'Sebagai salah satu ras anjing terbesar, Great Dane mencapai rata-rata berat badan 350 ? 450 gr ketika baru lahir. ', 'RC_Dog_Week1_06.jpg', 'a', 1, 1),
(32, 'Di bawah ini, mana yang bukan merupakan aktivitas yang harus diajarkan pada anjing sejak masih kecil?', 'Perawatan kebersihan', 'Mandi', 'Menyusu ', 'Perjalanan dengan mobil', 'Anjing belajar menyusui dari induknya langsung. Namun, Pet Owner tetap harus mengajarkan perawatan kebersihan, mandi, dan transportasi via mobil agar mereka terbiasa hidup teratur. ', 'RC_Dog_Week1_07.jpg', 'a', 1, 1),
(33, 'Apa tanda-tanda anjingmu akan melahirkan?', 'Keluarnya air susu di putting', 'Berjalan berputar-putar dan mencium lantai', 'Berjalan berputar-putar', 'Mengemis makanan', 'Salah satu tanda-tanda anjing betina yang akan melahirkan adalah keluarnya air susu di puting. ', 'RC_Dog_Week1_08.jpg', 'a', 1, 1),
(34, 'Berapa suhu tubuh normal bagi anak anjing?', '280 ? 290 C', '380 ? 390 C', '280 ? 290 C', '300 ? 350 C', 'Suhu tubuh normal bagi anak anjing adalah 380 ? 390 C', 'RC_Dog_Week1_09.jpg', 'a', 1, 1),
(35, 'Pada umur berapa anak anjing mulai disapih?', '1 ? 2 minggu', '3 ? 4 minggu', '1 bulan', '2 bulan', 'Masa sapih anak anjing dimulai pada usia 3-4 minggu', 'RC_Dog_Week1_10.jpg', 'a', 1, 1),
(36, 'Anak anjing akan menyusu pertama kali pada induknya?.?', 'Satu hari setelah lahir', 'Satu jam setelah lahir', 'Segera setelah lahir', 'Setengah jam setelah lahir', 'Segera setelah anak anjing lahir, anak anjing akan langsung\nmenyusu pada induknya.\n', 'RC_Dog_Week1_11.jpg', 'a', 1, 1),
(37, 'Kenapa sampo manusia tidak boleh digunakan untuk anjing?', 'pH kulit manusia dan anjing berbeda', 'Pertumbuhan rambut terhambat', 'Menyebabkan rambut mengembang', 'Membuat mata berair', 'Karena pH kulit manusia dan anjing berbeda, jangan pernah sekali\nkali menggunakan sampo manusia pada anjing karena dapat\nmenyebabkan iritasi pada kulit anjing.\n', 'RC_Dog_Week1_12.jpg', 'a', 1, 1),
(38, 'Di bawah ini, mana yang bukan cara melatih anjing untuk melakukan sesuatu?', 'Dengan suara keras dan lantang', 'Gunakan kata yang sama untuk perintah yang sama', 'Berikan isyarat tubuh', 'Peragakan dengan gerakan tangan', 'Jangan melatih anjingmu untuk melakukan sesuatu dengan suara\nkeras dan lantang karena anjing tidak akan mengerti perbedaannya.\n', 'RC_Dog_Week1_13.jpg', 'a', 1, 1),
(39, 'Bagaimana cara terbaik memberitahu anjing bahwa ia telah melakukan kesalahan?', 'Tidak memberi makan', 'Memukul', 'Membentak', 'Beritahu saat itu juga dengan lembut', 'Ketika anjingmu melakukan kesalahan, jangan membentak atau menghukum anjingmu karena anjing tidak akan mengerti dan hal itu hanya akan menyiksa anjing. Beritahulah begitu dia melakukan kesalahan dan beritahu dengan lembut.\n', 'RC_Dog_Week1_14.jpg', 'a', 1, 1),
(40, 'Pada umur berapakah gigi susu anjing mulai muncul?', '1 minggu', '2 minggu', '3 minggu', '4 minggu', 'Gigi susu anjing akan mulai muncul di usia 3 minggu', 'RC_Dog_Week1_15.jpg', 'a', 1, 1),
(41, 'Sisir seperti apa yang cocok untuk anjing berambut pendek?', 'Sisir sikat ?bergigi rapat?', 'Sisir kayu ?bergigi renggang?', 'Sisir plastik ?bergigi rapat?', 'Sisir logam ?bergigi renggang?', 'Gunakanlah sisir logam bergigi renggang untuk anjing dengan rambut pendek.\n', 'RC_Dog_Week1_16.tiff', 'a', 1, 1),
(42, 'Di antara jenis-jenis anjing berikut, mana yang memiliki rambut kaku?', 'Labrador', 'Yorkshire Terrier', 'Cavalier King Charles', 'Golden Retriever', 'Labrador memiliki jenis rambut yang pendek dan kaku. Sementara, Yorkshire Terrier, Cavalier King Charles, dan Golden Retriever semuanya memiliki rambut yang panjang.', 'RC_Dog_Week1_17.jpg', 'a', 1, 1),
(43, 'Apa manfaat berenang untuk anjing?', 'Menghilangkan iritasi pada kulit', 'Rambut lebih halus', 'Daya tahan tubuh lebih kuat', 'Nafsu makan bertambah', 'Berenang mempunyai manfaat untuk menghilangkan iritasi yang terjadi di kulit anjing. Tapi harus dipastikan anjing berenang di air yang aman untuk kulitnya karena pH kulit anjing berbeda dengan manusia. \n', 'RC_Dog_Week1_18.jpg', 'a', 1, 1),
(44, 'Faktor apa yang paling penting bagi anjing untuk memilih makanan mereka?', 'Bentuk ', 'Tekstur', 'Rasa', 'Aroma', 'Faktor terpenting untuk anjing dalam memilih makanan berbeda\ndengan faktor kucing dalam memilih makanan. Faktor terpenting\nbagi anjing adalah aroma dari makanan tersebut. \n', 'RC_Dog_Week1_19.jpg', 'a', 1, 1),
(45, 'Bagaimana ciri-ciri anjing yang ingin buang air?', 'Berjalan berputar-putar dan mencium lantai', 'Ekor mengibas-ngibas', 'Berguling-guling di lantai', 'Menggonggong', 'Anjing sebenarnya memberi tanda ketika ia ingin buang air. Salah\nsatu tanda paling umum adalah mereka berjalan berputar-putar dan\nmencium lantai untuk mencari tempat buang air.\n', 'RC_Dog_Week1_20.jpg', 'a', 1, 1),
(46, 'Mengapa anak anjing tidak boleh diajak jalan-jalan terlalu lama?', 'Mudah lelah', 'Tulang dan ototnya belum sempurna', 'Butuh waktu istirahat yang lama', 'Butuh waktu lama bersama induknya', 'Anak anjing belum memiliki tulang dan otot yang sempurna\nsehingga tidak boleh diajak beraktivitas terlalu berat seperti\nberjalan jauh atau dalam waktu yang lama. \n', 'RC_Dog_Week1_21.jpg', 'a', 1, 1),
(47, 'Di bawah ini, mana yang bukan kebutuhan anjing yang lebih sering tinggal di dalam ruangan? ', 'Latihan atau aktivitas fisik secara rutin', 'Makanan dengan kandungan energi sedang', 'Memantau berat badan', 'Perawatan gigi', 'Anjing indoor atau yang lebih sering tinggal di dalam ruangan\ncenderung untuk obesitas karena kekurangan aktivitas oleh karena\nitu mereka butuh aktivitas fisik secara rutin, makanan dengan\nkandungan energy yang sedang, dan dipantau berat badannya\nsecara teratur. \n', 'RC_Dog_Week1_22.jpg', 'a', 1, 1),
(48, 'Mengapa anjing harus diberikan makanan yang sama setiap harinya?', 'Menghindari gangguan pencernaan', 'Menghindari kelebihan berat badan', 'Menghindari kebiasaan mengemis makanan', 'Menghindari hilangnya nafsu makan', 'Anjing harus dibiasakan untuk diberikan makanan yang sama setiap hari agar terhindar dari gangguan pencernaan akibat makan makanan yang tidak sesuai dengan nutrisi kebutuhan anjing.', 'RC_Dog_Week1_23.jpg', 'a', 1, 1),
(49, 'Di antara jenis anjing berikut, mana yang ukuran tubuhnya sedang?', 'Pomeranian ', 'Beagle', 'Pekingese', 'Pug', 'Anjing Pomeranian, Pekingese, dan Pug semuanya berukuran tubuh mini.', 'RC_Dog_Week1_24.jpg', 'a', 1, 1),
(50, 'Di bawah ini, mana yang bukan cara mengatasi anjing yang suka makan barang-barang selain makanannya?', 'Mengajak berjalan-jalan', 'Mengajak bermain', 'Menambah aktivitas', 'Memberikan camilan', 'Anjing yang suka memakan barang-barang lain selain makanannya\nmemiliki energy berlebih yang harus disalurkan ke hal lain. Ajaklah\nanjing berjalan-jalan, bermain, dan dan beraktivitas.\n', 'RC_Dog_Week1_25.jpg', 'a', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `royalcanin__user`
--

CREATE TABLE IF NOT EXISTS `royalcanin__user` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `facebook_id` varchar(30) NOT NULL,
  `access_token` varchar(80) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=2 ;

--
-- Dumping data for table `royalcanin__user`
--

INSERT INTO `royalcanin__user` (`user_id`, `facebook_id`, `access_token`, `nama_lengkap`, `alamat`, `email`, `phone`, `datetime`) VALUES
(1, '10206915977003272', 'AAAGphKZBVACEBABQevEccJGHYjiacWeB1rj9ZCPmbwpCOH9LJwl2bZCZBJ1R7ZAXDp4AzFunaL5sRKq', 'Gilang Sonar', '', 'gilangsonar15@gmail.com', '087774323999', '2015-10-15 11:52:03');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
